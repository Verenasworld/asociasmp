package com.example.Asocia.DTO;

import com.example.Asocia.model.TagEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class GroupEntityDto implements Serializable {
    private final int groupId;
    private final String groupName;
    private final String groupDescription;
    private final String color;
    private final List<PostEntityDto> postList;
    private final List<UserEntityDto> userEntityList;
    private final List<TagEntity> tagEntityList;

    public GroupEntityDto(int groupId, String groupName, String groupDescription, String color, List<PostEntityDto> postList, List<UserEntityDto> userEntityList,List<TagEntity> tagEntityList) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupDescription = groupDescription;
        this.color = color;
        this.postList = postList;
        this.userEntityList = userEntityList;
        this.tagEntityList = tagEntityList;
    }

    public List<TagEntity> getTagEntityList() {
        return tagEntityList;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public String getColor() {
        return color;
    }

    public List<PostEntityDto> getPostList() {
        return postList;
    }

    public List<UserEntityDto> getUserEntityList() {
        return userEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupEntityDto entity = (GroupEntityDto) o;
        return Objects.equals(this.groupId, entity.groupId) &&
                Objects.equals(this.groupName, entity.groupName) &&
                Objects.equals(this.groupDescription, entity.groupDescription) &&
                Objects.equals(this.color, entity.color) &&
                Objects.equals(this.postList, entity.postList) &&
                Objects.equals(this.userEntityList, entity.userEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, groupName, groupDescription, color, postList, userEntityList);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "groupId = " + groupId + ", " +
                "groupName = " + groupName + ", " +
                "groupDescription = " + groupDescription + ", " +
                "color = " + color + ", " +
                "postList = " + postList + ", " +
                "userEntityList = " + userEntityList + ")";
    }
}
