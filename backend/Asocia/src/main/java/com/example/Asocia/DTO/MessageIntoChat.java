package com.example.Asocia.DTO;

public class MessageIntoChat {

    private int userId;
    private String messageText;
    private int chatId;

    public MessageIntoChat(int userId, String messageText, int chatId) {
        this.userId = userId;
        this.messageText = messageText;
        this.chatId = chatId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }
}
