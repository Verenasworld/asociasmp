package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.LikeEntity;
import org.springframework.data.repository.CrudRepository;

public interface LikeCRUDRepository extends CrudRepository<LikeEntity, Integer> {
}
