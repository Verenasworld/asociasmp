package com.example.Asocia.DTO;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class ChatEntityDto implements Serializable {
    private final int chatId;
    private final List<MessageEntityDto> messageEntityList;
    private final UserEntityDto userEntity;

    public ChatEntityDto(int chatId, List<MessageEntityDto> messageEntityList, UserEntityDto userEntity) {
        this.chatId = chatId;
        this.messageEntityList = messageEntityList;
        this.userEntity = userEntity;
    }

    public int getChatId() {
        return chatId;
    }

    public List<MessageEntityDto> getMessageEntityList() {
        return messageEntityList;
    }

    public UserEntityDto getUserEntity() {
        return userEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatEntityDto entity = (ChatEntityDto) o;
        return Objects.equals(this.chatId, entity.chatId) &&
                Objects.equals(this.messageEntityList, entity.messageEntityList) &&
                Objects.equals(this.userEntity, entity.userEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatId, messageEntityList, userEntity);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "chatId = " + chatId + ", " +
                "messageEntityList = " + messageEntityList + ", " +
                "userEntityList = " + userEntity + ")";
    }
}
