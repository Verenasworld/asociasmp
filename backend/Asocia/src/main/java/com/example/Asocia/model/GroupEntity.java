package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class GroupEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int groupId;

    @Column
    private String groupName;

    @Column(length = 100000000)
    private String groupDescription;

    @Column
    private String color;

    @Column
    private int groupAdmin;

    @OneToMany(mappedBy = "groupEntity" , cascade = CascadeType.ALL)
    @JsonManagedReference(value = "groupPost")
    private List<PostEntity> postList = new ArrayList<>();


    @ManyToMany(mappedBy = "groupEntityTagList" ,cascade = CascadeType.ALL)
    //@JsonManagedReference(value = "userGroup")
    @JsonIgnoreProperties("groupEntityTagList")
    private List<TagEntity> tagEntityList = new ArrayList<>();

    @ManyToMany
    //@JsonBackReference(value = "userGroup")
    @JsonIgnoreProperties("groupEntityList")
    @JoinTable(
            name = "UserGroup",
            joinColumns = {@JoinColumn(name = "groupId")},
            inverseJoinColumns = {@JoinColumn(name="userId")})
    private List<UserEntity> userEntityList = new ArrayList<>();

    public GroupEntity(String groupName, String groupDescription, String color, int groupAdmin) {
        this.groupName = groupName;
        this.groupDescription = groupDescription;
        this.color = color;
        this.groupAdmin = groupAdmin;
    }
    public GroupEntity() {}


    public int getGroupAdmin() {
        return groupAdmin;
    }

    public void setGroupAdmin(int groupAdmin) {
        this.groupAdmin = groupAdmin;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<PostEntity> getPostList() {
        return postList;
    }

    public void setPostList(PostEntity postList) {
        this.postList.add(postList);
    }



    public List<UserEntity> getUserEntityList() {
        return userEntityList;
    }

    /*public void setUserEntityList(List<UserEntity> userEntityList) {
        this.userEntityList = userEntityList;
    }*/
    public void setUserEntityInToGroup(UserEntity userEntity) {
        this.userEntityList.add(userEntity);
    }

    public List<TagEntity> getTagEntityList() {
        return tagEntityList;
    }

    public void setTagEntityList(List<TagEntity> tagEntityList) {
        this.tagEntityList = tagEntityList;
    }
}
