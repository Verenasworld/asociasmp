package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ChatEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int chatId;

    @OneToMany(mappedBy = "chatEntity")
    @JsonManagedReference(value = "message")
    private List<MessageEntity> messageEntityList = new ArrayList<>();

    @ManyToMany
    //@JsonBackReference(value = "chat")
    @JsonIgnoreProperties("chat")
    @JoinTable(
            name = "PrivateChat",
            joinColumns = {@JoinColumn(name = "chatId")},
            inverseJoinColumns = {@JoinColumn(name="userId")})
    private List<UserEntity> userEntityList = new ArrayList<>();

    public ChatEntity() {}

    public ChatEntity(List<MessageEntity> messageEntityList) {
        this.messageEntityList = messageEntityList;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {this.chatId = chatId;}

    public List<MessageEntity> getMessageEntityList() {
        return messageEntityList;
    }

    public void setMessageEntityList(MessageEntity messageEntityList) {
        this.messageEntityList.add(messageEntityList);
    }

    public List<UserEntity> getUserEntityList() {
        return userEntityList;
    }

    public void setUserEntityList(UserEntity userEntityList) {
        this.userEntityList.add(userEntityList);
    }
}
