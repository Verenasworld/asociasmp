package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class PostEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int postId;

    @Column
    private String postImage;

    @Column(length = 999999)
    private String postText;

    @Column
    private Date postDate;

    @ManyToOne
    @JsonBackReference(value = "userPost")
    @JoinColumn(name = "userId")
    private UserEntity userEntity;

    @ManyToOne
    @JsonBackReference(value = "groupPost")
    @JoinColumn(name = "asociaGroupId")
    private GroupEntity groupEntity;

    @OneToMany(mappedBy = "postLike", cascade = CascadeType.ALL)
    @JsonManagedReference(value = "postLike")
    private List<LikeEntity> likeList = new ArrayList<>();

    @OneToMany(mappedBy = "postComment", cascade = CascadeType.ALL)
    @JsonManagedReference(value = "postComment")
    private List<CommentEntity> commentEntityList = new ArrayList<>();

    public PostEntity() {}
    public PostEntity(String postImage, String postText, Date postDate) {
        this.postImage = postImage;
        this.postText = postText;
        this.postDate = postDate;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public GroupEntity getGroupEntity() {
        return groupEntity;
    }

    public void setGroupEntity(GroupEntity groupEntity) {
        this.groupEntity = groupEntity;
    }

    public List<LikeEntity> getLikeList() {
        return likeList;
    }

    public void setLikeList(LikeEntity likeList) {
        this.likeList.add(likeList);
    }

    public List<CommentEntity> getCommentEntityList() {
        return commentEntityList;
    }

    public void setCommentEntityList(CommentEntity commentEntity) {
        this.commentEntityList.add(commentEntity);
    }
}

