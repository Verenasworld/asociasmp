package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TagEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int tagId;

    @Column
    private String color;

    @Column
    private String text;

    /*@ManyToOne
    @JsonBackReference(value = "groupTag")
    @JoinColumn(name = "groupId")
    private GroupEntity groupEntityTag;*/

    @ManyToMany(cascade = CascadeType.ALL)
    //@JsonBackReference(value = "chat")
    @JsonIgnoreProperties("groupEntityList")
    @JoinTable(
            name = "tags",
            joinColumns = {@JoinColumn(name = "tagId")},
            inverseJoinColumns = {@JoinColumn(name="groupId")})
    private List<GroupEntity> groupEntityTagList = new ArrayList<>();

    public TagEntity(String color, String text) {
        this.color = color;
        this.text = text;
    }
    public TagEntity() {}

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int adId) {
        this.tagId = tagId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<GroupEntity> getGroupEntityTagList() {
        return groupEntityTagList;
    }

    public void setGroupEntityTagList(GroupEntity groupEntityTagList) {
        this.groupEntityTagList.add(groupEntityTagList);
    }
}
