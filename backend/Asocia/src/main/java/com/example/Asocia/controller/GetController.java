package com.example.Asocia.controller;

import com.example.Asocia.CRUDrepository.GroupCRUDRepository;
import com.example.Asocia.CRUDrepository.PostCRUDRepository;
import com.example.Asocia.CRUDrepository.TagCRUDRepository;
import com.example.Asocia.CRUDrepository.UserCRUDRepository;
import com.example.Asocia.DTO.*;
import com.example.Asocia.FileService.FilesStorageService;
import com.example.Asocia.FileService.ResponseMessage;
import com.example.Asocia.model.*;
import com.example.Asocia.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.text.html.HTML;
import java.util.*;

@RestController
@RequestMapping("/api")
public class GetController {

    @Autowired
    private UserCRUDRepository userCRUDRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private GroupCRUDRepository groupCRUDRepository;

    @Autowired
    FilesStorageService storageService;

    @Autowired
    private PostCRUDRepository postCRUDRepository;

    @Autowired
    private TagCRUDRepository tagCRUDRepository;


    @PostMapping("/uploadPost/{groupId}/{userId}/{postText}")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file,
                                                      @PathVariable int groupId,
                                                      @PathVariable int userId,
                                                      @PathVariable String postText) {

        UserEntity userEntity = userCRUDRepository.findById(userId).get();
        GroupEntity groupEntity = groupCRUDRepository.findById(groupId).get();
        String fileName = userEntity.getEmail()+"post"+Math.random();
        String message = "";
        try {
            storageService.save(file, fileName);
            PostEntity postEntity = new PostEntity("http://localhost:8081/api/files/"+fileName+".png", postText, new Date());
            postEntity.setGroupEntity(groupEntity);
            postEntity.setUserEntity(userEntity);
            postCRUDRepository.save(postEntity);
            userEntity.setPostList(postEntity);
            userCRUDRepository.save(userEntity);
            groupEntity.setPostList(postEntity);
            groupCRUDRepository.save(groupEntity);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/addGroup/{id}")
    public void createGroup(@RequestBody GroupEntity groupEntity, @PathVariable int id) {
        GroupEntity newGroup = new GroupEntity(groupEntity.getGroupName(), groupEntity.getGroupDescription(), groupEntity.getColor(), id);
        groupCRUDRepository.save(newGroup);
        List<TagEntity> tagEntityList = new ArrayList<>();
        for (int i = 0; i < groupEntity.getTagEntityList().size(); i++) {
            TagEntity newTag = new TagEntity(groupEntity.getTagEntityList().get(i).getColor(),groupEntity.getTagEntityList().get(i).getText());
            newTag.setGroupEntityTagList(newGroup);
            tagCRUDRepository.save(newTag);
            tagEntityList.add(newTag);
        }

        newGroup.setTagEntityList(tagEntityList);
        newGroup.setUserEntityInToGroup(userCRUDRepository.findById(id).get());
        groupCRUDRepository.save(newGroup);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.PUT, path = "/updateUser")
    public void updateUser(@RequestBody UserEntity userEntity) {
        UserEntity ue = userCRUDRepository.findByEmail(userEntity.getEmail());
        ue.setFirstname(userEntity.getFirstname());
        ue.setLastname(userEntity.getLastname());
        ue.setWohnort(userEntity.getWohnort());
        ue.setUserImage(userEntity.getUserImage());
        ue.setStatus(userEntity.getStatus());
        userCRUDRepository.save(ue);
    }

   @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/register")
    /**
     * tries to add new useraccount
     * return true if successfull
     */
   public boolean register(@RequestBody UserEntity user) {
        boolean registered = userRepository.insertNewUserIn(user.getFirstname(),user.getLastname(),user.getEmail(),user.getWohnort(), encoder.encode(user.getUserPassword()), "Willkommen bei Aso");
        return registered;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/signedInUser")
    /**
     * returns username of signed it use or null if noone is signed in
     */
    public UserPostEntityDto getSignedInUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getName().equals("anonymousUser") ) {  //springsecurity is creating "anonymousUser" if login was not succesfull
            return null;
        } else {

            return userRepository.getUserPosts(userCRUDRepository.findByEmail(authentication.getName()).getUserId());
        }
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/logout")
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/AllUser")
    public List<UserEntity> getAllUserQ(){
        Query query = em.createQuery("SELECT u FROM UserEntity u");
        List results = query.getResultList();
        return results;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/AllUser/{id}")
    public List<UserEntity> getAllUser(@PathVariable int id){
        UserEntity userEntity = userCRUDRepository.findById(id).get();
        List<UserEntity> userEntityList = new ArrayList<>();
        for (UserEntity uet:userCRUDRepository.findAll()) {
            userEntityList.add(uet);
        }
        for (UserEntity ue :userCRUDRepository.findAll()) {
            if (ue.getUserId() == userEntity.getUserId() || ue.getUserId() == userCRUDRepository.findByUserName("s.system").getUserId()){
                userEntityList.remove(ue);
            }
        }
        return userEntityList;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/AllTags")
    public List<TagEntity> AllTags(){
        Query query = em.createQuery("SELECT distinct(t.text) , t.color FROM TagEntity t");
        List results = query.getResultList();
        return results;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/getUser/{id}")
    public UserEntity getUser(@PathVariable int id){
        return userCRUDRepository.findById(id).get();
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/AllGroups")
    public List<GroupEntity> getAllGroups(){
        Query query = em.createQuery("SELECT g FROM GroupEntity g");
        List results = query.getResultList();
        return results;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/Group/{id}")
    public GroupEntityDto getGroupDto(@PathVariable int id){
        GroupEntity groupEntity = groupCRUDRepository.findById(id).get();
        List<PostEntityDto> postEntityDtoList = new ArrayList<>();
        List<UserEntityDto> userEntityDtoList = new ArrayList<>();
        List<TagEntity> tagEntityList = new ArrayList<>();
        List<LikeEntityDto> likeEntityDtoList;
        List<CommentEntityDto> commentEntityDtos;
        for (UserEntity ue:groupEntity.getUserEntityList()) {
            userEntityDtoList.add(getUserEntityToDto(ue));
        }
        for (PostEntity pe:groupEntity.getPostList()) {
            likeEntityDtoList = new ArrayList<>();
            commentEntityDtos = new ArrayList<>();

            for (LikeEntity le: pe.getLikeList()) {
                likeEntityDtoList.add(new LikeEntityDto(
                        le.getLikeId(),le.getLikeDate(),
                        getUserEntityToDto(le.getUserEntity())));
            }
            for (CommentEntity le: pe.getCommentEntityList()) {
                commentEntityDtos.add(new CommentEntityDto(
                        le.getCommentId(),
                        le.getCommentText(),
                        le.getCommentDate(),
                        getUserEntityToDto(le.getUserEntity())));
            }
            postEntityDtoList.add(new PostEntityDto(
                    pe.getPostId(),
                    pe.getPostImage(),
                    pe.getPostText(),
                    getUserEntityToDto(pe.getUserEntity()),
                    likeEntityDtoList,
                    pe.getPostDate(),
                    commentEntityDtos));
        }
        GroupEntityDto groupEntityDto = new GroupEntityDto(
                groupEntity.getGroupId(),
                groupEntity.getGroupName(),
                groupEntity.getGroupDescription(),
                groupEntity.getColor(),
                postEntityDtoList,
                userEntityDtoList,
                groupEntity.getTagEntityList());
        return groupEntityDto;
    }

    private UserEntityDto getUserEntityToDto(UserEntity pe) {
        return new UserEntityDto(
                pe.getUserId(),
                pe.getFirstname(),
                pe.getLastname(),
                pe.getEmail(),
                pe.getUserImage(),
                pe.getUserName(),
                pe.getStatus());
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/getUserPosts/{id}")
    public UserPostEntityDto getUserPosts(@PathVariable int id){
        UserEntity ue = userCRUDRepository.findById(id).get();
        List<PostEntityDto> postEntityDtoList = new ArrayList<>();
        List<LikeEntityDto> likeEntityDtoList;
        List<CommentEntityDto> commentEntityDtos;
        for (PostEntity pe:ue.getPostList()) {
            likeEntityDtoList = new ArrayList<>();
            commentEntityDtos = new ArrayList<>();
            for (LikeEntity le:pe.getLikeList()) {
                likeEntityDtoList.add(new LikeEntityDto(le.getLikeId(),le.getLikeDate(), getUserEntityToDto(le.getUserEntity())));
            }
            for (CommentEntity ce:pe.getCommentEntityList()) {
                commentEntityDtos.add(new CommentEntityDto(ce.getCommentId(),ce.getCommentText(),ce.getCommentDate(), getUserEntityToDto(ce.getUserEntity())));
            }
            postEntityDtoList.add(new PostEntityDto(
                    pe.getPostId(),
                    pe.getPostImage(),
                    pe.getPostText(),
                    getUserEntityToDto(pe.getUserEntity()),
                    likeEntityDtoList,
                    pe.getPostDate(),
                    commentEntityDtos));
        }
        UserPostEntityDto postEntityDto = new UserPostEntityDto(
                ue.getUserId(),
                ue.getFirstname(),
                ue.getLastname(),
                ue.getEmail(),
                ue.getWohnort(),
                ue.getStatus(),
                ue.getUserImage(),
                ue.getUserName(),postEntityDtoList,ue.getGroupEntityList());
        return postEntityDto;
    }
}
