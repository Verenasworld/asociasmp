package com.example.Asocia.controller;

import com.example.Asocia.CRUDrepository.ChatEntityRepository;
import com.example.Asocia.CRUDrepository.MessageCRUDRepository;
import com.example.Asocia.CRUDrepository.UserCRUDRepository;
import com.example.Asocia.DTO.*;
import com.example.Asocia.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ChatController {

    @Autowired
    UserCRUDRepository userCRUDRepository;

    @Autowired
    ChatEntityRepository chatEntityRepository;

    @Autowired
    MessageCRUDRepository messageCRUDRepository;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/PrivateChat/{loggedUserid}/{userId}")
    public ChatEntity postPrivateChat(@PathVariable int loggedUserid, @PathVariable int userId) {
        ChatEntity newChat;
        UserEntity loggedUser = userCRUDRepository.findById(loggedUserid).get();
        UserEntity user = userCRUDRepository.findById(userId).get();
        for (ChatEntity ce :loggedUser.getChat()) {
            for (ChatEntity ce2:user.getChat()) {
                if (ce.getChatId() == ce2.getChatId()){
                    return ce;
                }
            }
        }
        newChat = new ChatEntity();
        newChat.setUserEntityList(user);
        newChat.setUserEntityList(loggedUser);
        MessageEntity me = new MessageEntity(
                "Willkommen im Chat mit "+user.getFirstname()+" & "+loggedUser.getFirstname(),
                new Date(),
                userCRUDRepository.findByEmail("System@s.s").getUserId());
        messageCRUDRepository.save(me);
        newChat.setMessageEntityList(me);
        chatEntityRepository.save(newChat);
        me.setChatEntity(newChat);
        messageCRUDRepository.save(me);
        loggedUser.setChat(newChat);
        user.setChat(newChat);
        return newChat;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.PUT, path = "/PrivateChatMessage")
    public ChatEntity putMessageInChat(@RequestBody MessageIntoChat messageIntoChat) {
        ChatEntity chatEntity = chatEntityRepository.findById(messageIntoChat.getChatId()).get();
        MessageEntity messageEntity = new MessageEntity(messageIntoChat.getMessageText(), new Date(), messageIntoChat.getUserId());
        messageEntity.setChatEntity(chatEntity);
        messageCRUDRepository.save(messageEntity);
        chatEntity.setMessageEntityList(messageEntity);
        chatEntityRepository.save(chatEntity);
        return chatEntity;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/Chats/{id}")
    public List<ChatEntityDto> putMessageInChat(@PathVariable int id) {
        UserEntity userEntity = userCRUDRepository.findById(id).get();
        List<ChatEntityDto> chatEntityDtoList = new ArrayList<>();
        List<MessageEntityDto> messageEntityDtoList;
        UserEntityDto userEntityDto = null;
        for (ChatEntity ce:userEntity.getChat()) {
            messageEntityDtoList = new ArrayList<>();
            for (MessageEntity me:ce.getMessageEntityList()) {
                messageEntityDtoList.add(new MessageEntityDto(
                        me.getMessageId(),
                        me.getMessageText(),
                        me.getMessageDate(),
                        me.getUserId()));
            }
            for (UserEntity ue:ce.getUserEntityList()) {
                if (ue.getUserId() != userEntity.getUserId()){
                    userEntityDto = new UserEntityDto(
                            ue.getUserId(),
                            ue.getFirstname(),
                            ue.getLastname(),
                            ue.getEmail(),
                            ue.getUserImage(),
                            ue.getUserName(),
                            ue.getStatus());
                }
            }
            chatEntityDtoList.add(new ChatEntityDto(ce.getChatId(),messageEntityDtoList,userEntityDto));
        }
        return chatEntityDtoList;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/UpdateChat/{chatId}")
    public ChatEntity updateChatMessage(@PathVariable int chatId) {
        return chatEntityRepository.findById(chatId).get();
    }




}
