package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.apache.catalina.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;

    @Column
    private String firstname;

    @Column
    private String lastname;

    @Column
    private String email;

    @Column
    private String wohnort;

    @Column
    private String status;

    @Column
    private String userImage;

    @Column
    private String userName;

    @Column
    private String userPassword;

    public UserEntity() {
    }

    public UserEntity(String firstname, String lastname, String email, String wohnort, String userImage, String userName, String userPassword, String status) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.wohnort = wohnort;
        this.userImage = userImage;
        this.userName = userName;
        this.userPassword = userPassword;
        this.status = status;
    }

    @OneToMany(mappedBy = "userEntity")
    @JsonManagedReference(value = "userLike")
    private List<LikeEntity> likeList = new ArrayList<>();

    @OneToMany(mappedBy = "userEntity")
    @JsonManagedReference(value = "userPost")
    private List<PostEntity> postList = new ArrayList<>();

    @OneToMany(mappedBy = "userEntity")
    @JsonManagedReference(value = "userComment")
    private List<CommentEntity> commentList = new ArrayList<>();

    @ManyToMany(mappedBy = "userEntityList")
    //@JsonManagedReference(value = "userGroup")
    @JsonIgnoreProperties("userEntityList")
    private List<GroupEntity> groupEntityList = new ArrayList<>();

    @ManyToMany(mappedBy = "userEntityList")
    //@JsonManagedReference(value = "userComment")
    @JsonIgnoreProperties("userEntityList")
    private List<ChatEntity> chat = new ArrayList<>();

    public List<ChatEntity> getChat() {
        return chat;
    }

    public void setChat(ChatEntity chat) {
        this.chat.add(chat);
    }

    public String getStatus() {return status;}
    public void setStatus(String status) {this.status = status;}

    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getWohnort() {
        return wohnort;
    }
    public void setWohnort(String wohnort) {
        this.wohnort = wohnort;
    }

    public String getUserImage() {
        return userImage;
    }
    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public List<LikeEntity> getLikeList() {
        return likeList;
    }
    public void setLikeList(List<LikeEntity> likeList) {
        this.likeList = likeList;
    }

    public List<PostEntity> getPostList() {
        return postList;
    }
    public void setPostList(PostEntity postList) {
        this.postList.add(postList);
    }

    public List<CommentEntity> getCommentList() {
        return commentList;
    }
    public void setCommentList(CommentEntity commentList) {
        this.commentList.add(commentList);
    }

    public List<GroupEntity> getGroupEntityList() {return groupEntityList;}
    public void setGroupEntityList(List<GroupEntity> groupEntityList) {
        this.groupEntityList = groupEntityList;
    }

}
