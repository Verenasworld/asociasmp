package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.GroupEntity;
import org.springframework.data.repository.CrudRepository;

public interface GroupCRUDRepository extends CrudRepository<GroupEntity, Integer> {
}
