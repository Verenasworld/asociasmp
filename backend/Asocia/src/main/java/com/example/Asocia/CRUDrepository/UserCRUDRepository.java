package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.ChatEntity;
import com.example.Asocia.model.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserCRUDRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findByUserName(String userName);
    UserEntity findByEmail(String email);

    UserEntity findByChat(ChatEntity chat);






}
