package com.example.Asocia.controller;

import com.example.Asocia.CRUDrepository.*;
import com.example.Asocia.DTO.CommentToPostDto;
import com.example.Asocia.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PostController {

    @Autowired
    private UserCRUDRepository userCRUDRepository;

    @Autowired
    private GroupCRUDRepository groupCRUDRepository;
    @Autowired
    private PostCRUDRepository postCRUDRepository;

    @Autowired
    private CommentCRUDRepository commentCRUDRepository;

    @Autowired
    private LikeCRUDRepository likeCRUDRepository;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/userIntoGroup/{id}")
    public void setUserIntoGroup(@RequestBody List<String> userEmails, @PathVariable int id) {
        GroupEntity groupEntity = groupCRUDRepository.findById(id).get();
        for (String userEmail:userEmails) {
            groupEntity.setUserEntityInToGroup(userCRUDRepository.findByEmail(userEmail));
        }
        groupCRUDRepository.save(groupEntity);
    }
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/allGroups/{id}")
    public List<GroupEntity> filterGroups(@PathVariable int id) {
        UserEntity userEntity = userCRUDRepository.findById(id).get();
        List<GroupEntity> groupEntityList = new ArrayList<>();
        for (GroupEntity uet:groupCRUDRepository.findAll()) {
            groupEntityList.add(uet);
        }
        for (GroupEntity ge :userEntity.getGroupEntityList()) {
            for (GroupEntity ge1:groupCRUDRepository.findAll()) {
                if (ge.getGroupId() == ge1.getGroupId()){
                    groupEntityList.remove(ge1);
                }
            }
        }
        return groupEntityList;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/userIntoGroup/{id}")
    public List<UserEntity> getAllUsersForGroup(@PathVariable int id) {
        List<UserEntity> groupUserList = groupCRUDRepository.findById(id).get().getUserEntityList();
        Iterable<UserEntity> allUserList = userCRUDRepository.findAll();
        List<UserEntity> userList = new ArrayList<>();
        for (UserEntity uet:allUserList) {
            userList.add(uet);
        }
        for (UserEntity ue: allUserList) {
            for (UserEntity ueg: groupUserList) {
                if (ue.equals(ueg)){
                    userList.remove(ue);
                }
            }
        }
        return userList;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/Comment")
    public void postCommentToPost(@RequestBody CommentToPostDto commentToPostDto) {
        PostEntity postEntity = postCRUDRepository.findById(commentToPostDto.getPostId()).get();
        UserEntity userEntity = userCRUDRepository.findById(commentToPostDto.getUserId()).get();
        CommentEntity commentEntity = new CommentEntity(commentToPostDto.getCommentText(), new Date());
        commentEntity.setPostComment(postEntity);
        commentEntity.setUserEntity(userEntity);
        commentCRUDRepository.save(commentEntity);
        postEntity.setCommentEntityList(commentEntity);
        userEntity.setCommentList(commentEntity);
        postCRUDRepository.save(postEntity);
        userCRUDRepository.save(userEntity);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/Like/{postId}/{userId}")
    public void likeToPost(@PathVariable int postId, @PathVariable int userId) {
        UserEntity ue = userCRUDRepository.findById(userId).get();
        PostEntity pe = postCRUDRepository.findById(postId).get();
        LikeEntity le = new LikeEntity(new Date());
        le.setUserEntity(ue);
        le.setPostLike(pe);
        likeCRUDRepository.save(le);
        pe.setLikeList(le);
        postCRUDRepository.save(pe);
    }
}
