package com.example.Asocia.DTO;

import java.io.Serializable;
import java.util.Objects;

public class UserEntityDto implements Serializable {
    private final int userId;
    private final String firstname;
    private final String lastname;
    private final String email;
    private final String userImage;
    private final String userName;

    private final String status;

    public UserEntityDto(int userId, String firstname, String lastname, String email, String userImage, String userName, String status) {
        this.userId = userId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.userImage = userImage;
        this.userName = userName;
        this.status  = status;
    }

    public String getStatus() {
        return status;
    }

    public int getUserId() {
        return userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntityDto entity = (UserEntityDto) o;
        return Objects.equals(this.userId, entity.userId) &&
                Objects.equals(this.firstname, entity.firstname) &&
                Objects.equals(this.lastname, entity.lastname) &&
                Objects.equals(this.email, entity.email) &&
                Objects.equals(this.userImage, entity.userImage) &&
                Objects.equals(this.userName, entity.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, firstname, lastname, email, userImage, userName);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "userId = " + userId + ", " +
                "firstname = " + firstname + ", " +
                "lastname = " + lastname + ", " +
                "email = " + email + ", " +
                "userImage = " + userImage + ", " +
                "userName = " + userName + ")";
    }
}
