package com.example.Asocia.DTO;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class PostEntityDto implements Serializable {
    private final int postId;
    private final String postImage;
    private final String postText;

    private final Date postDate;
    private final UserEntityDto userEntity;
    private final List<LikeEntityDto> likeList;

    private final List<CommentEntityDto> commentList;

    public PostEntityDto(int postId, String postImage, String postText, UserEntityDto userEntity, List<LikeEntityDto> likeList,  Date postDate, List<CommentEntityDto> commentList) {
        this.postId = postId;
        this.postImage = postImage;
        this.postText = postText;
        this.userEntity = userEntity;
        this.likeList = likeList;
        this.postDate = postDate;
        this.commentList = commentList;
    }

    public List<CommentEntityDto> getCommentList() {
        return commentList;
    }

    public Date getPostDate() {
        return postDate;
    }

    public int getPostId() {
        return postId;
    }

    public String getPostImage() {
        return postImage;
    }

    public String getPostText() {
        return postText;
    }

    public UserEntityDto getUserEntity() {
        return userEntity;
    }

    public List<LikeEntityDto> getLikeList() {
        return likeList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PostEntityDto entity = (PostEntityDto) o;
        return Objects.equals(this.postId, entity.postId) &&
                Objects.equals(this.postImage, entity.postImage) &&
                Objects.equals(this.postText, entity.postText) &&
                Objects.equals(this.userEntity, entity.userEntity) &&
                Objects.equals(this.likeList, entity.likeList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, postImage, postText, userEntity, likeList);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "postId = " + postId + ", " +
                "postImage = " + postImage + ", " +
                "postText = " + postText + ", " +
                "userEntity = " + userEntity + ", " +
                "likeList = " + likeList + ")";
    }
}
