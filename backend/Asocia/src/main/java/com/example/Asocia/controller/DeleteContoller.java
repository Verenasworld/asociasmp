package com.example.Asocia.controller;


import com.example.Asocia.CRUDrepository.GroupCRUDRepository;
import com.example.Asocia.CRUDrepository.PostCRUDRepository;
import com.example.Asocia.model.GroupEntity;
import com.example.Asocia.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Query;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DeleteContoller {

    @Autowired
    GroupCRUDRepository groupCRUDRepository;

    @Autowired
    PostCRUDRepository postCRUDRepository;


    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, path = "/DeleteGroup/{id}")
    public void deleteGroup(@PathVariable int id){
        groupCRUDRepository.deleteById(id);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.DELETE, path = "/DeletePost/{id}")
    public void deletePost(@PathVariable int id){
        postCRUDRepository.deleteById(id);
    }
}
