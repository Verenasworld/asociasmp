package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.PostEntity;
import org.springframework.data.repository.CrudRepository;

public interface PostCRUDRepository extends CrudRepository<PostEntity, Integer> {
}
