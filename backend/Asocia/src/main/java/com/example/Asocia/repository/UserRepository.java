package com.example.Asocia.repository;

import com.example.Asocia.CRUDrepository.UserCRUDRepository;
import com.example.Asocia.DTO.*;
import com.example.Asocia.model.CommentEntity;
import com.example.Asocia.model.LikeEntity;
import com.example.Asocia.model.PostEntity;
import com.example.Asocia.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Repository
@Transactional
public class UserRepository {


    @Autowired
    UserCRUDRepository userCRUDRepository;

    //public UserEntity(String firstname, String lastname, String email, String wohnort, String userImage, String userName, String userPassword) {


        //Registrierung
    //überprüft, ob der Name schon vergeben wurde - falls nicht, speichert neue UserIn in DB
    //TODO hashen?
    public boolean insertNewUserIn(String firstname, String lastname,String email, String wohnort, String userPasswort, String status) {
        for(UserEntity userIn : userCRUDRepository.findAll()) {
            if(userIn.getEmail().equals(email))
                return false;
        }
        String userName = firstname+"."+lastname;
        //userCRUDRepository.save(new UserEntity(firstname, lastname, email, wohnort,"",userName, userPasswort, ));
        userCRUDRepository.save(new UserEntity(firstname, lastname, email, wohnort, "avatar7.png", userName, userPasswort, status));
        return true;
    }

    public boolean insertNewUserIn(UserEntity userEntity) {
        for(UserEntity userIn : userCRUDRepository.findAll()) {
            if(userIn.getEmail().equals(userEntity.getEmail()))
                return false;
        }
        String userName = userEntity.getFirstname()+"."+userEntity.getLastname();
        userCRUDRepository.save(userEntity);
        return true;
    }


    public UserPostEntityDto getUserPosts(int userId){
        UserEntity ue = userCRUDRepository.findById(userId).get();
        List<PostEntityDto> postEntityDtoList = new ArrayList<>();
        List<LikeEntityDto> likeEntityDtoList;
        List<CommentEntityDto> commentEntityDtos;
        for (PostEntity pe:ue.getPostList()) {
            likeEntityDtoList = new ArrayList<>();
            commentEntityDtos = new ArrayList<>();
            for (LikeEntity le:pe.getLikeList()) {
                likeEntityDtoList.add(new LikeEntityDto(le.getLikeId(),le.getLikeDate(), getUserEntityToDto(le.getUserEntity())));
            }
            for (CommentEntity ce:pe.getCommentEntityList()) {
                commentEntityDtos.add(new CommentEntityDto(ce.getCommentId(),ce.getCommentText(),ce.getCommentDate(), getUserEntityToDto(ce.getUserEntity())));
            }
            postEntityDtoList.add(new PostEntityDto(
                    pe.getPostId(),
                    pe.getPostImage(),
                    pe.getPostText(),
                    getUserEntityToDto(pe.getUserEntity()),
                    likeEntityDtoList,
                    pe.getPostDate(),
                    commentEntityDtos));
        }
        UserPostEntityDto postEntityDto = new UserPostEntityDto(
                ue.getUserId(),
                ue.getFirstname(),
                ue.getLastname(),
                ue.getEmail(),
                ue.getWohnort(),
                ue.getStatus(),
                ue.getUserImage(),
                ue.getUserName(),postEntityDtoList,ue.getGroupEntityList());
        return postEntityDto;
    }

    private UserEntityDto getUserEntityToDto(UserEntity pe) {
        return new UserEntityDto(
                pe.getUserId(),
                pe.getFirstname(),
                pe.getLastname(),
                pe.getEmail(),
                pe.getUserImage(),
                pe.getUserName(),
                pe.getStatus());
    }




}
