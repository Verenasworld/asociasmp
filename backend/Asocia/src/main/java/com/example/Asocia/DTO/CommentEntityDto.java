package com.example.Asocia.DTO;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class CommentEntityDto implements Serializable {
    private final int commentId;
    private final String commentText;
    private final Date commentDate;
    private final UserEntityDto userEntity;

    public CommentEntityDto(int commentId, String commentText, Date commentDate, UserEntityDto userEntity) {
        this.commentId = commentId;
        this.commentText = commentText;
        this.commentDate = commentDate;
        this.userEntity = userEntity;
    }

    public int getCommentId() {
        return commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public UserEntityDto getUserEntity() {
        return userEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommentEntityDto entity = (CommentEntityDto) o;
        return Objects.equals(this.commentId, entity.commentId) &&
                Objects.equals(this.commentText, entity.commentText) &&
                Objects.equals(this.commentDate, entity.commentDate) &&
                Objects.equals(this.userEntity, entity.userEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(commentId, commentText, commentDate, userEntity);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "commentId = " + commentId + ", " +
                "commentText = " + commentText + ", " +
                "commentDate = " + commentDate + ", " +
                "userEntity = " + userEntity + ")";
    }
}
