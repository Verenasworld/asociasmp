package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.MessageEntity;
import org.springframework.data.repository.CrudRepository;

public interface MessageCRUDRepository extends CrudRepository<MessageEntity, Integer> {
}
