package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.text.DateFormat;
import java.util.Date;

@Entity
public class LikeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int likeId;

    @Column
    private Date likeDate;


    @ManyToOne
    @JsonBackReference(value = "userLike")
    @JoinColumn(name = "UserId")
    private UserEntity userEntity;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference(value = "postLike")
    @JoinColumn(name = "postId")
    private PostEntity postLike;

    public LikeEntity(Date likeDate) {
        this.likeDate = likeDate;
    }


    public LikeEntity() {}

    public int getLikeId() {
        return likeId;
    }

    public void setLikeId(int likeId) {
        this.likeId = likeId;
    }

    public Date getLikeDate() {
        return likeDate;
    }

    public void setLikeDate(Date likeDate) {
        this.likeDate = likeDate;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public PostEntity getPostLike() {
        return postLike;
    }

    public void setPostLike(PostEntity postLike) {
        this.postLike = postLike;
    }
}
