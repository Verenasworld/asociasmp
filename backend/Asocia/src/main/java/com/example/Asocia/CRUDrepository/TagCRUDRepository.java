package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.TagEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TagCRUDRepository extends CrudRepository<TagEntity, Integer> {
    TagEntity findByText(String text);

    @Query("select distinct t from TagEntity t where t.text = ?1")
    List<TagEntity> findDistinctByText(String text);








}
