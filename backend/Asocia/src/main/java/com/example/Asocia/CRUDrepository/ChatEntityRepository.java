package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.ChatEntity;
import com.example.Asocia.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ChatEntityRepository extends CrudRepository<ChatEntity, Integer> {
    ChatEntity findByUserEntityList(UserEntity userEntityList);


}