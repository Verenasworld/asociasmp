package com.example.Asocia.DTO;

public class CommentToPostDto {

    private String commentText;
    private int userId;
    private int postId;

    public CommentToPostDto(String commentText, int userId, int postId) {
        this.commentText = commentText;
        this.userId = userId;
        this.postId = postId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
