package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

@Entity
public class CommentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int commentId;

    @Column(length = 999999)
    private String commentText;

    @Column
    private Date commentDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonBackReference(value = "postComment")
    @JoinColumn(name = "PostId")
    private PostEntity postComment;

    @ManyToOne
    @JsonBackReference(value = "userComment")
    @JoinColumn(name = "UserId")
    private UserEntity userEntity;

    public CommentEntity(String commentText, Date commentDate) {
        this.commentText = commentText;
        this.commentDate = commentDate;
    }

    public CommentEntity() {}

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    public PostEntity getPostComment() {
        return postComment;
    }

    public void setPostComment(PostEntity postComment) {
        this.postComment = postComment;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}
