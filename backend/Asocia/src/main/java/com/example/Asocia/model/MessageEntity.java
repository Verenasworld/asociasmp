package com.example.Asocia.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;

@Entity
public class MessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int messageId;

    @Column(length = 999999)
    private String messageText;

    @Column
    private Date messageDate;

    @Column
    private int  userId;

    @ManyToOne
    @JsonBackReference(value = "message")
    @JoinColumn(name = "chat")
    private ChatEntity chatEntity;

    public MessageEntity() {}

    public MessageEntity(String messageText, Date messageDate, int userId) {
        this.messageText = messageText;
        this.messageDate = messageDate;
        this.userId = userId;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public ChatEntity getChatEntity() {
        return chatEntity;
    }

    public void setChatEntity(ChatEntity chatEntity) {
        this.chatEntity = chatEntity;
    }
}
