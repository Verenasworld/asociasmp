package com.example.Asocia.DTO;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.Objects;

public class LikeEntityDto implements Serializable {
    private final int likeId;
    private final Date likeDate;
    private final UserEntityDto userEntity;

    public LikeEntityDto(int likeId, Date likeDate, UserEntityDto userEntity) {
        this.likeId = likeId;
        this.likeDate = likeDate;
        this.userEntity = userEntity;
    }

    public int getLikeId() {
        return likeId;
    }

    public Date getLikeDate() {
        return likeDate;
    }

    public UserEntityDto getUserEntity() {
        return userEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LikeEntityDto entity = (LikeEntityDto) o;
        return Objects.equals(this.likeId, entity.likeId) &&
                Objects.equals(this.likeDate, entity.likeDate) &&
                Objects.equals(this.userEntity, entity.userEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(likeId, likeDate, userEntity);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "likeId = " + likeId + ", " +
                "likeDate = " + likeDate + ", " +
                "userEntity = " + userEntity + ")";
    }
}
