package com.example.Asocia.DTO;

import com.example.Asocia.model.GroupEntity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class UserPostEntityDto implements Serializable {
    private final int userId;
    private final String firstname;
    private final String lastname;
    private final String email;
    private final String wohnort;
    private final String status;
    private final String userImage;
    private final String userName;
    private final List<PostEntityDto> postList;
    private final List<GroupEntity> groupEntityList;

    public UserPostEntityDto(int userId, String firstname, String lastname, String email, String wohnort, String status, String userImage, String userName, List<PostEntityDto> postList, List<GroupEntity> groupEntityList) {
        this.userId = userId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.wohnort = wohnort;
        this.status = status;
        this.userImage = userImage;
        this.userName = userName;
        this.postList = postList;
        this.groupEntityList = groupEntityList;
    }



    public int getUserId() {
        return userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getWohnort() {
        return wohnort;
    }

    public String getStatus() {
        return status;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getUserName() {
        return userName;
    }

    public List<PostEntityDto> getPostList() {
        return postList;
    }

    public List<GroupEntity> getGroupEntityList() {
        return groupEntityList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPostEntityDto entity = (UserPostEntityDto) o;
        return Objects.equals(this.userId, entity.userId) &&
                Objects.equals(this.firstname, entity.firstname) &&
                Objects.equals(this.lastname, entity.lastname) &&
                Objects.equals(this.email, entity.email) &&
                Objects.equals(this.wohnort, entity.wohnort) &&
                Objects.equals(this.status, entity.status) &&
                Objects.equals(this.userImage, entity.userImage) &&
                Objects.equals(this.userName, entity.userName) &&
                Objects.equals(this.postList, entity.postList) &&
                Objects.equals(this.groupEntityList, entity.groupEntityList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, firstname, lastname, email, wohnort, status, userImage, userName, postList, groupEntityList);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "userId = " + userId + ", " +
                "firstname = " + firstname + ", " +
                "lastname = " + lastname + ", " +
                "email = " + email + ", " +
                "wohnort = " + wohnort + ", " +
                "status = " + status + ", " +
                "userImage = " + userImage + ", " +
                "userName = " + userName + ", " +
                "postList = " + postList + ", " +
                "groupEntityList = " + groupEntityList + ")";
    }
}
