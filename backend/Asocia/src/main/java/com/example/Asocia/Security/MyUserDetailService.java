package com.example.Asocia.Security;

import com.example.Asocia.model.UserEntity;
import com.example.Asocia.CRUDrepository.UserCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.ArrayList;

@Service
@Transactional
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserCRUDRepository userCRUDRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userin = userCRUDRepository.findByEmail(email);  //find  userin with name "username" in our database (h2,mysql,)
        if(userin==null){
            throw new UsernameNotFoundException("No user found with username:" +email);
        }
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();

        //hard coding a role for every user. (could also be retrieved from the database for advanced authority-management)
        authorities.add(new SimpleGrantedAuthority("ROLE_USER")); //Must be written with "ROLE_"-prefix (for reasons...)

        //Create spring security user with values from our userin
        User springSecUser = new User(userin.getEmail(),(userin.getUserPassword()),authorities);
        return springSecUser;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder(); //NoOpPasswordEncoder.getInstance();
    }
}
