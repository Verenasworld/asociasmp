package com.example.Asocia.CRUDrepository;

import com.example.Asocia.model.CommentEntity;
import org.springframework.data.repository.CrudRepository;

public interface CommentCRUDRepository extends CrudRepository<CommentEntity, Integer> {
}
