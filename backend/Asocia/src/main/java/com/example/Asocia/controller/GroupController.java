package com.example.Asocia.controller;


import com.example.Asocia.CRUDrepository.ChatEntityRepository;
import com.example.Asocia.CRUDrepository.GroupCRUDRepository;
import com.example.Asocia.CRUDrepository.UserCRUDRepository;
import com.example.Asocia.model.ChatEntity;
import com.example.Asocia.model.GroupEntity;
import com.example.Asocia.model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GroupController {

    @Autowired
    ChatEntityRepository chatEntityRepository;

    @Autowired
    UserCRUDRepository userCRUDRepository;

    @Autowired
    GroupCRUDRepository groupCRUDRepository;

    @CrossOrigin
    @RequestMapping(method = RequestMethod.PUT, path = "/JoinGroup/{userId}/{groupId}")
    public void updateChatMessage(@PathVariable int groupId, @PathVariable int userId) {
        GroupEntity ge = groupCRUDRepository.findById(groupId).get();
        ge.setUserEntityInToGroup(userCRUDRepository.findById(userId).get());
        groupCRUDRepository.save(ge);
    }
}
