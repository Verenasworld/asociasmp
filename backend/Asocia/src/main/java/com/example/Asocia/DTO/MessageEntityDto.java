package com.example.Asocia.DTO;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class MessageEntityDto implements Serializable {
    private final int messageId;
    private final String messageText;
    private final Date messageDate;
    private final int userId;

    public MessageEntityDto(int messageId, String messageText, Date messageDate, int userId) {
        this.messageId = messageId;
        this.messageText = messageText;
        this.messageDate = messageDate;
        this.userId = userId;
    }

    public int getMessageId() {
        return messageId;
    }

    public String getMessageText() {
        return messageText;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageEntityDto entity = (MessageEntityDto) o;
        return Objects.equals(this.messageId, entity.messageId) &&
                Objects.equals(this.messageText, entity.messageText) &&
                Objects.equals(this.messageDate, entity.messageDate) &&
                Objects.equals(this.userId, entity.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId, messageText, messageDate, userId);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "messageId = " + messageId + ", " +
                "messageText = " + messageText + ", " +
                "messageDate = " + messageDate + ", " +
                "userId = " + userId + ")";
    }
}
