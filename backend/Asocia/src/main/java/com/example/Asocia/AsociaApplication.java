package com.example.Asocia;

import com.example.Asocia.CRUDrepository.*;
import com.example.Asocia.FileService.FilesStorageService;
import com.example.Asocia.model.*;
import com.example.Asocia.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.Resource;
import java.util.Date;

@SpringBootApplication
public class AsociaApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(AsociaApplication.class, args);
	}


	@Autowired
	UserCRUDRepository userCRUDRepository;

	@Autowired
	GroupCRUDRepository groupCRUDRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	MessageCRUDRepository messageCRUDRepository;

	@Autowired
	ChatEntityRepository chatEntityRepository;

	@Autowired
	private PasswordEncoder encoder;

	@Resource
	FilesStorageService storageService;

	@Autowired
	LikeCRUDRepository likeCRUDRepository;

	@Override
	public void run(String... args) throws Exception {
		storageService.deleteAll();
		storageService.init();



		UserEntity userStevan = new UserEntity("Stevan","Mijucic","stevann@live.at","Wien","avatar13.png","m.stevan",encoder.encode("hallo1234"), "ich bin supa");

		UserEntity u1 = new UserEntity("Mareike", "Fuchs", "f.mareike@gmail.com", "Wien", "avatar8.png", "f.Mareike", encoder.encode("testtest"), "Willkommen bei Asocia");
		UserEntity u2 = new UserEntity("Danilo", "Vujicic", "v.danilo@gmail.com", "Wien", "avatar1.png", "v.Danilo", encoder.encode("testtest"), "Willkommen bei Asocia");
		UserEntity u3 = new UserEntity("Ulrich","Fadl", "f.ulrich@gmail.com", "Wien", "avatar10.png", "f.Ulrich", encoder.encode("testtest"), "Willkommen bei Asocia");
		UserEntity u4 = new UserEntity("Niklas","Koller", "niki34@gmail.com", "Wien", "avatar3.png", "Niklas", encoder.encode("testtest"), "Willkommen bei Asocia");
		UserEntity u5 = new UserEntity("Selda","A", "selda@gmail.com", "Wien", "avatar4.png", "selda", encoder.encode("testtest"),"Willkommen bei Asocia" );
		UserEntity u6 = new UserEntity("Tom","Bob", "tom@gmail.com", "Wien", "avatar10.png", "tom", encoder.encode("testtest"),"Willkommen bei Asocia" );


		UserEntity systemUser = new UserEntity("System","System","System@s.s", "", "avatar7.png", "s.system",encoder.encode("testtest"),"");
		userCRUDRepository.save(systemUser);
		userRepository.insertNewUserIn(u1);
		userRepository.insertNewUserIn(u2);
		userRepository.insertNewUserIn(u3);
		userRepository.insertNewUserIn(u4);
		userRepository.insertNewUserIn(u5);
		userRepository.insertNewUserIn(u6);

		userRepository.insertNewUserIn(userStevan);

		//userRepository.insertNewUserIn(userStevan.getFirstname(), userStevan.getLastname(), userStevan.getEmail(),userStevan.getWohnort(),userStevan.getUserPassword());

		UserEntity userVerena = new UserEntity("Verena","Schink","verena_schink@gmail.at","Wien","avatar2.png","s.verena",encoder.encode("123456789"), "ich bin supa");
		//userRepository.insertNewUserIn(userVerena.getFirstname(), userVerena.getLastname(), userVerena.getEmail(),userVerena.getWohnort(),userVerena.getUserPassword());
		userRepository.insertNewUserIn(userVerena);

		ChatEntity chatEntity1 = new ChatEntity();
		MessageEntity messageEntity = new MessageEntity("servas",new Date(), 1);

		messageCRUDRepository.save(messageEntity);


		chatEntity1.setMessageEntityList(messageEntity);
		//chatEntity1.setUserEntityList(userStevan);
		chatEntityRepository.save(chatEntity1);
		messageEntity.setChatEntity(chatEntity1);
		messageCRUDRepository.save(messageEntity);


		userStevan.setChat(chatEntity1);
		userCRUDRepository.save(userStevan);


		GroupEntity groupFitness = new GroupEntity("Fitness", "Fitness Fitness Fitness Fitness Fitness Fitness Fitness Fitness Fitness ","blue", userCRUDRepository.findByEmail("verena_schink@gmail.at").getUserId());
		groupFitness.setUserEntityInToGroup(userCRUDRepository.findByEmail("verena_schink@gmail.at"));
		GroupEntity groupBurgerKing = new GroupEntity("BrugerKing", "Burger BurgerBurgerBurgerBurger","red",userCRUDRepository.findByEmail("stevann@live.at").getUserId());
		GroupEntity groupMarvel = new GroupEntity("Marvel", "Spiderman bla bla bla bla bla bla bla","green",userCRUDRepository.findByEmail("stevann@live.at").getUserId());
		GroupEntity groupJava = new GroupEntity("Java", "Fahrzeig Beispiel","red",userCRUDRepository.findByEmail("verena_schink@gmail.at").getUserId());
		GroupEntity groupCss = new GroupEntity("Css", "Margin padding und so","red",userCRUDRepository.findByEmail("verena_schink@gmail.at").getUserId());

		groupCRUDRepository.save(groupJava);
		groupCRUDRepository.save(groupCss);
		groupBurgerKing.setUserEntityInToGroup(userCRUDRepository.findByEmail("stevann@live.at"));
		groupFitness.setUserEntityInToGroup(userCRUDRepository.findByEmail("stevann@live.at"));
		groupMarvel.setUserEntityInToGroup(userCRUDRepository.findByEmail("stevann@live.at"));

		groupCRUDRepository.save(groupFitness);
		groupCRUDRepository.save(groupBurgerKing);
		groupCRUDRepository.save(groupMarvel);




	}

}
