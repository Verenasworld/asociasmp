import Vue from "vue";
import VueRouter from "vue-router";

import LandingPage from "@/view/LandingPage";
import LoginRegistration from "@/view/LoginRegistration";
import UserDashboard from "@/view/UserDashboard";
import UserEdit from "@/components/user/UserEdit";
import UserGroups from "@/components/user/UserGroups";
import UserFriends from "@/components/user/UserFriends";
import MyChat from "@/components/user/MyChat";
import SmpOverview from "@/components/smp/SmpOverview";
import GroupOverview from "@/components/GroupOverview/GroupOverview";
import ShowUsers from "@/components/ShowUsers";
import UserProfil from "@/components/user/UserProfil";
import PrivatChat from "@/components/chat/PrivatChat";
import BusinessProfile from "@/view/BusinessProfil";
import MyGroups from "@/components/GroupOverview/MyGroups";


Vue.use(VueRouter)

let routes = [
    {
        path: '/',
        name: 'landing-page',
        component : LandingPage

    },
    {
        path:'/login',
        name: 'login-registration',
        component: LoginRegistration
    },
    {
        path: '/user',
        name: 'user-dashboard',
        component: UserDashboard,
        children:[
            {
                path:'/',
                component: GroupOverview,
            },
            {
                path:'/smp-overview/:id',
                component: SmpOverview,
            },
            {
                path: '/user-profile/',
                component: UserProfil,
            },
            {
                path: '/privat-chat/',
                component: PrivatChat,
            },

        ]
    },
    {
        path: '/userEdit',
        name: 'user-edit',
        component: UserEdit
    },
    {
        path: '/userGroup',
        name: 'user-groups',
        component: UserGroups
    },
    {
        path: '/userFriends',
        name: 'user-friends',
        component: UserFriends
    },
    {
        path: '/myChat',
        name: 'my-chat',
        component: MyChat
    },
    {
        path: '/showUsers',
        name: 'show-users',
        component: ShowUsers
    },
    {
        path: '/businessProfile',
        name: 'business-profile',
        component: BusinessProfile
    },
    {

        path: '/myGroups',
        name: 'myGroups',
        component: MyGroups

    }


]

let router = new VueRouter({
    routes
})

export default router
