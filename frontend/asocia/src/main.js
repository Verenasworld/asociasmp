import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import axios from "axios";
import VueAxios from "vue-axios";
import router from "@/router";
import store from "./store";
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import 'vue-swatches/dist/vue-swatches.css';

Vue.use(VueAxios,axios,vuetify,router)
Vue.config.productionTip = false
async function createVueApp() {
  await store.dispatch('getSignedInUser')
new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
}
createVueApp()
