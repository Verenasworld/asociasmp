import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
        state: {
            user: null,
            users: [],
            imgFiles: null,
            selectedGroup: '',
            groups: [],
            group: '',
            filterUsers: [],
            userInfo: '',
            chatUserInfo: null,
            chatDetails: '',
            messages: [],
            AllChats: [],
            allTags: [],
            chatId:0
        },
        mutations: {
            setTags(state, tags) {
                state.allTags = []
                for (let i = 0; i < tags.length; i++) {
                    state.allTags.push({
                        text: tags[i][0],
                        color: tags[i][1]
                    })
                }
                console.log(state.allTags)


            },
            setChatUserInfo(state, user) {
                state.chatUserInfo = user
            },
            setUser(state, user) {
                state.user = user
            },
            setAllUsers(state, users) {
                state.users = users
                state.filterUsers = users
            },
            setImgFiles(state, files) {
                state.imgFiles = files
            },
            setAllGroups(state, group) {
                state.groups = group
            },
            setSelectedGroup(state, group) {
                state.selectedGroup = group
            },
            setAllUsersForGroup(state, users) {
                state.filterUsers = users
            },
            setUserProfilInfo(state, userInfo) {
                state.userInfo = userInfo
            },
            setChatId(state, chatDetails) {
                state.chatDetails = chatDetails
                state.messages = chatDetails.messageEntityList
                state.chatId = chatDetails.chatId
            },

            setChats(state, chats) {
                state.AllChats = chats
            }
        },
        actions: {
            async login(context, credentials) {
                let basicAuth = 'Basic ' + btoa(credentials.email + ':' + credentials.userPassword)
                const response = await Vue.axios.get('/api/signedInUser', {
                    headers: {'Authorization': basicAuth}
                })
                console.log(response.data)
                context.commit('setUser', response.data)
            },
            async allUser(context) {
                const response = await Vue.axios.get('/api/AllUser/'+ this.state.user.userId)
                context.commit('setAllUsers', response.data)
            },
            async register(context, newUserData) {
                await Vue.axios.post('/api/register', newUserData)
            },
            async getSignedInUser(context) {
                const response = await Vue.axios.get('/api/signedInUser')
                context.commit('setUser', response.data)
            },
            async logout(context) {
                console.log("logout")
                await Vue.axios.post('/api/logout')
                context.commit('setUser', null)
            },
            async loadAllUsers(context) {
                const response = await Vue.axios.get('/api/users')
                context.commit('setAllUsers', response.data)
            },
            async addGroup(context, groupInfo) {
                const response = await Vue.axios.post('/api/addGroup/' + this.state.user.userId, groupInfo)
                console.log(response)
            },
            async addFile(context, file) {
                let formData = new FormData();
                formData.append("file", file);
                console.log(file)
                const response = await Vue.axios.post('/api/upload', formData, {headers: {"Content-Type": "mulitipart/form-data",}})
                console.log(response)
            },
            async addPost(context, file) {
                let formData = new FormData();
                formData.append("file", file.file);
                console.log(file)
                const response = await Vue.axios.post('/api/uploadPost/'
                    + file.groupId + '/'
                    + this.state.user.userId +
                    '/' + file.description,
                    formData,
                    {headers: {"Content-Type": "mulitipart/form-data",}})
                console.log(response)
            },
            async getFile(context) {
                const response = await Vue.axios.get('/api/files')
                console.log(response)
                context.commit('setImgFiles', response.data)
            },
            async allGroups(context) {
                const response = await Vue.axios.get('/api/AllGroups')
                console.log(response.data)
                context.commit('setAllGroups', response.data)
            },
            async selectedGroup(context, id) {
                const response = await Vue.axios.get('/api/Group/' + id)
                console.log(response.data)
                context.commit('setSelectedGroup', response.data)
            },
            async setUserInfo(context, user) {
                const response = await Vue.axios.put('/api/updateUser', user)
                console.log(response.data)
            },
            async setUserIntoGroup(context, users) {
                const response = await Vue.axios.post('/api/userIntoGroup/' + users.id, users.userEmails)
                console.log(response.data)
            },
            async getAllUsersForGroup(context, id) {
                const response = await Vue.axios.get('/api/userIntoGroup/' + id)
                console.log(response.data)
                context.commit('setAllUsersForGroup', response.data)
            },
            async postCommentIntoPost(context, commentInfo) {
                const response = await Vue.axios.post('/api/Comment/', commentInfo)
                console.log(response.data)
            },
            /*async getUserProfilInfo(context){
                const response = await Vue.axios.get('/api/getUserPosts/'+this.state.user.userId )
                context.commit('setUserProfilInfo', response.data)
                console.log(response.data)
            },*/
            async privateChat(context, id) {
                const response = await Vue.axios.post('/api/PrivateChat/' + this.state.user.userId + '/' + id)
                console.log(response.data)
                context.commit("setChatId", response.data)
            },
            async sendMessage(context, messageText) {
                const response = await Vue.axios.put('/api/PrivateChatMessage', {
                    messageText: messageText,
                    userId: this.state.user.userId,
                    chatId: this.state.chatDetails.chatId
                })
                console.log(response.data)
                context.commit("setChatId", response.data)
            },
            async deleteGroup(context, id) {
                const response = await Vue.axios.delete('/api/DeleteGroup/' + id)
                console.log(response.data)
            },
            async deletePost(context, id) {
                const response = await Vue.axios.delete('/api/DeletePost/' + id)
                console.log(response.data)
            },
            async getAllGroups(context) {
                const response = await Vue.axios.get('/api/allGroups/' + this.state.user.userId)
                console.log(response.data)
                context.commit('setAllGroups', response.data)
            },
            async getChats(context) {
                const response = await Vue.axios.get('/api/Chats/' + this.state.user.userId)
                console.log(response.data)
                context.commit('setChats', response.data)
            },
            async getAllTags(context) {
                const response = await Vue.axios.get('/api/AllTags/')
                context.commit('setTags', response.data)
            },
            async updateChat(context) {
                const response = await Vue.axios.get('/api/UpdateChat/' + this.state.chatId)
                console.log(response.data)
                context.commit("setChatId", response.data)
            },
            async joinGroup(context, groupId){
                const response = await Vue.axios.put('/api/JoinGroup/' + this.state.user.userId+"/"+groupId)
                console.log(response)
            },
            async likeToPost(context, postId){
                const response = await Vue.axios.post('/api/Like/'+postId+"/" + this.state.user.userId)
                console.log(response)
            },
        }
    }
)
